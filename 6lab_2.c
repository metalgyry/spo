// gcc -o /home/egor/Desktop/6_2lab  '/home/egor/Desktop/6lab_2.c' -lm
// /home/egor/Desktop/6_2lab

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
int msgid;
struct mymsg{
	double mdata1;
	double mdata2;
	double mdata3;
	int z1;
	int z2;
	int z3;
} m;

int main(){
double A,B,C,D,E,F;
double x,y;
system("ipcs -q");
msgid = msgget(IPC_PRIVATE, IPC_CREAT|0666);
if (msgid<0) { fprintf(stdout,"\nОшибка"); return 0; }
system("ipcs -q");
if (fork() == 0){
	system("ipcs -q");
	sleep(1);
	printf("\nPress A: ");
	scanf("%lf",&A);
	if(A == 0){
		m.z1=1;
		m.mdata1=1;
	}else{
		m.z1=2;
		m.mdata1=A;
	}
	printf("\nAA= %f",A);
	printf("\nPress B: ");
	scanf("%lf",&B);
	if(B == 0){
		m.z2=1;
		m.mdata2=1;
	}else{
		m.z2=2;
		m.mdata2=B;
	}
	printf("\nBB= %f",B);
	printf("\nPress C: ");
	scanf("%lf",&C);
	if(C == 0){
		m.z3=1;
		m.mdata3=1;
	}else{
		m.z3=2;
		m.mdata3=C;
	}
	printf("\nCC= %f",C);
	msgsnd(msgid,&m,sizeof(struct mymsg),0);
	return 1;	
}
if (fork() == 0){
	FILE *fi;
	fi = fopen("/home/egor/Desktop/6_2_text", "r");
	fscanf(fi, "%le", &D);
	if(D == 0){
		m.z1=1;
		m.mdata1=1;
	}else{
		m.z1=2;
		m.mdata1=D;
	}
	printf("\nDD= %f",D);
	fscanf(fi, "%le", &E);
	if(E == 0){
		m.z2=1;
		m.mdata2=1;
	}else{
		m.z2=2;
		m.mdata2=E;
	}
	printf("\nEE= %f",E);
	fscanf(fi, "%le", &F);
	if(F == 0){
		m.z3=1;
		m.mdata3=1;
	}else{
		m.z3=2;
		m.mdata3=F;
	}
	printf("\nFF= %f",F);
	msgsnd(msgid,&m,sizeof(struct mymsg),0);
	fclose(fi);
	return 1;	
}
sleep(10);
system("ipcs -q");
msgrcv(msgid,&m,sizeof(struct mymsg),0,0);
if(m.z1 == 1){
	D = 0;
}else{
	D = m.mdata1;
}
printf("\nD= %f",D);
if(m.z2 == 1){
	E = 0;
}else{
	E = m.mdata2;
}
printf("\nE= %f",E);
if(m.z3 == 1){
	F = 0;
}else{
	F = m.mdata3;
}
printf("\nF= %f",F);
msgrcv(msgid,&m,sizeof(struct mymsg),0,0);
if(m.z1 == 1){
	A = 0;
}else{
	A = m.mdata1;
}
printf("\nA= %f",A);
if(m.z2 == 1){
	B = 0;
}else{
	B = m.mdata2;
}
printf("\nB= %f",B);
if(m.z3 == 1){
	C = 0;
}else{
	C = m.mdata3;
}
printf("\nC= %f",C);
y = ((D*C)-(F*A))/((E*A)-(D*B));
printf("\ny= %f",y);
x = (0-B*y-C)/A;
printf("\nx= %f\n",x);
msgctl(msgid, IPC_RMID, NULL);
system("ipcs -q");
return 0;
}
