// gcc -o /home/egor/Desktop/5_22lab  '/home/egor/Desktop/5_2(2)_lab.c' -lm
// /home/egor/Desktop/5_22lab

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <sched.h>
#include <linux/sched.h>
#include <unistd.h>
#define NUMSTACK 5000
char stack[100][NUMSTACK];
int n,k;
int mas[100][100];
int masstr[100];
int param[100];

int func(void *param){
	int buf = 0;
	int par = *((int*)param);
	double res;
	for(int j = 0; j < k; j++){
		res = mas[par][j];
		double res1 = sqrt(res);
		double res2 = floor(sqrt(res));
		if((res1-res2) == 0 ){
			buf+=1;
		}
	}
	masstr[par] = buf;
}

int main(){
int bufi, buf = 0;
printf("Press n: ");
scanf("%d",&n);
while(n < 0){
	printf("\nPress n again: ");
	scanf("%d",&n);
}
printf("\nPress k: ");
scanf("%d",&k);
while(k < 0){
	printf("\nPress k again: ");
	scanf("%d",&k);
}
printf("\nPress elements mas:\n");
for(int i = 0; i < n; i++){
	for(int j = 0; j < k; j++){
		scanf("%d", &buf);
		mas[i][j] = buf;
	}
}

printf("\nmas:\n");
for(int i = 0; i < n; i++){
	for(int j = 0; j < k; j++){
		buf = mas[i][j];
		printf("%d ", buf);
	}
	printf("\n");
}

for (int i=0 ; i < (n-1) ; i++){
	param[i]=i;
	char *tostack=stack[i];
	clone(func,(void*)(tostack+NUMSTACK-1),CLONE_VM,(void*)(param+i));
}
param[n-1]=n-1;
char *tostack=stack[n-1];
clone(func,(void*)(tostack+NUMSTACK-1),CLONE_VM|CLONE_VFORK,(void*)(param+n-1));
buf = -1;
for(int i = 0; i < n; i++){
	printf("\n%d-%d",masstr[i],i);
}
for(int i = 0; i < n; i++){
	if(masstr[i] > buf){
	buf = masstr[i];
	bufi = i;
	}
}
printf("\nstring number %d\n", (bufi+1));
return 0;
}
