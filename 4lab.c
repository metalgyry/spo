// gcc -o /home/egor/Desktop/4lab '/home/egor/Desktop/4lab.c'
// /home/egor/Desktop/4lab

//int pid = getpid();
//fork ();
//if(fork() == pid){
//	fork() ;
//}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(){
int ch;
printf("Press number(1,2,3,4): ");
scanf("%d",&ch);
puts("\n");
if(ch == 1){
	if(fork() == 0){
		char str[255];
		printf("Press string: ");
		fgets(str, 255, stdin);
		system(str);
	}
}
if(ch == 2){
	if(fork() == 0){
		puts("\n");
		system("/home/egor/firstscript.sh");
	}
}
if(ch == 3){
	if(fork() == 0){
		system("cat /etc/passwd | awk -F':' {'print $1'}");
		//system("compgen -u");
	}
}
if(ch == 4){
	puts("exit");
	return 0;
}

return 0;
}
