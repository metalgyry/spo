// gcc -o /home/egor/Desktop/5lab  '/home/egor/Desktop/5_1_lab.c' -lm
// /home/egor/Desktop/5lab

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/wait.h>
int k;
int shmid;
int semid;
struct sembuf Plus1 = {0, 1, 0};
struct sembuf Minus1 = {0, -1, 0};

struct mm{
	int max;
	int min;
} *mm_o;

void search(int i, int (*p)[k]){
	for(int j = 0; j < k; j++){
		if( ((i+j)%2) == 0 ){
			if( p[i][j] > mm_o->max){
				semop( semid, &Minus1, 1);
				system("ipcs -s");
				mm_o->max = p[i][j];
				semop( semid, &Plus1, 1);
				system("ipcs -s");
			}
			if(p[i][j] < mm_o->min){
				semop( semid, &Minus1, 1);
				system("ipcs -s");
				mm_o->min = p[i][j];
				semop( semid, &Plus1, 1);
				system("ipcs -s");
			}
		}
	}
}

int main(){
int n, buf, buff;
shmid = shmget(IPC_PRIVATE, 4, IPC_CREAT|0666);
if (shmid < 0 ) { fprintf(stdout,"\nОшибка"); return 0; }
semid = semget (IPC_PRIVATE, 1, IPC_CREAT|0666);
if (semid < 0 ) { fprintf(stdout, "\nОшибка"); return 0; }
semop( semid, &Plus1, 1);
mm_o = (struct mm *)shmat(shmid,NULL,0);
mm_o->max = INT_MIN;
mm_o->min = INT_MAX;
printf("Press n: ");
scanf("%d",&n);
while(n < 0){
	printf("\nPress n again: ");
	scanf("%d",&n);
}
printf("\nPress k: ");
scanf("%d",&k);
while(k < 0){
	printf("\nPress k again: ");
	scanf("%d",&k);
}
int masA[n][k];
int (*p)[k] = masA;
printf("\nPress elements masA:\n");
for(int i = 0; i < n; i++){
	for(int j = 0; j < k; j++){
		scanf("%d",&buf);
		p[i][j] = buf;
	}
}
printf("\nmasA:\n");
for(int i = 0; i < n; i++){
	for(int j = 0; j < k; j++){
	buff = p[i][j];
		printf("%d ", buff);
	}
	printf("\n");
}

for (int i=0; i < (n-1); i++)
{ 
	if (fork() == 0){
		search(i, p);
		return 0;
	}
}
search(n-1, p);
for (int i=0 ; i<3 ; i++){
	wait(NULL);
}

printf("\nMax: %d\n", mm_o->max);
printf("\nMin: %d\n", mm_o->min);
//system("ipcs -s");

return 0;
}
