// gcc -o /home/egor/Desktop/6_2lab  '/home/egor/Desktop/6lab_2.c' -lm
// /home/egor/Desktop/6_2lab

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
int msgid;
struct mymsg{
	int mtype;
	int mdata;
} m;

int main(){
double A,B,C,D,E,F;
double x,y;
//system("ipcs -q");
msgid = msgget(IPC_PRIVATE, IPC_CREAT|0666);
if (msgid<0) { fprintf(stdout,"\nОшибка"); return 0; }
//system("ipcs -q");
if (fork() == 0){
	sleep(1);
	printf("\nPress A: ");
	scanf("%lf",&A);
	m.mtype = 1;
	m.mdata=A;
	printf("\nAA= %f",A);
	msgsnd(msgid,&m,2,0);
	printf("\nPress B: ");
	scanf("%lf",&B);
	m.mtype = 1;
	m.mdata=B;
	printf("\nBB= %f",B);
	msgsnd(msgid,&m,2,0);
	printf("\nPress C: ");
	scanf("%lf",&C);
	m.mtype = 1;
	m.mdata=C;
	printf("\nCC= %f",C);
	msgsnd(msgid,&m,2,0);
	//system("ipcs -q");
	return 1;	
}
if (fork() == 0){
	FILE *fi;
	fi = fopen("/home/egor/Desktop/6_2_text", "r");
	fscanf(fi, "%le", &D);
	m.mtype = 1;
	m.mdata=D;
	printf("\nDD= %f",D);
	msgsnd(msgid,&m,2,0);
	//system("ipcs");
	fscanf(fi, "%le", &E);
	m.mtype = 1;
	m.mdata=E;
	printf("\nEE= %f",E);
	msgsnd(msgid,&m,2,0);
	//system("ipcs");
	fscanf(fi, "%le", &F);
	m.mtype = 1;
	m.mdata=F;
	printf("\nFF= %f",F);
	msgsnd(msgid,&m,2,0);
	//system("ipcs");
	fclose(fi);
	return 1;	
}
sleep(10);
msgrcv(msgid,&m,2,0,0);
D = m.mdata;
printf("\nD= %f",D);
msgrcv(msgid,&m,2,0,0);
E = m.mdata;
printf("\nE= %f",E);
msgrcv(msgid,&m,2,0,0);
F = m.mdata;
printf("\nF= %f",F);
msgrcv(msgid,&m,2,0,0);//system("ipcs");
A = m.mdata;
printf("\nA= %f",A);
msgrcv(msgid,&m,2,0,0);//system("ipcs");
B = m.mdata;
printf("\nB= %f",B);
msgrcv(msgid,&m,2,0,0);//system("ipcs");
C = m.mdata;
printf("\nC= %f",C);
y = ((D*C)-(F*A))/((E*A)-(D*B));
printf("\ny= %f",y);
x = (0-B*y-C)/A;
printf("\nx= %f",x);
msgctl(msgid, IPC_RMID, NULL);
return 0;
}
