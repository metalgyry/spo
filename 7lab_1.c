// gcc -o /home/egor/Desktop/7_1lab  '/home/egor/Desktop/7lab_1.c' -lm
// /home/egor/Desktop/7_1lab

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
int msgid, n, k;
int masA[100][100];
int masB[100][100];

struct mem_mas{
	int memory;
	int x;
	int y;
} mn;

void last_out(int signum){
	for(int f = 0; f < k; f++){
		for(int c = 0; c < n; c++){
			printf("{%d,%d} -- ", c,f);
			msgrcv(msgid,&mn,sizeof(struct mem_mas),0,0);
			printf("number = %d; ", mn.memory);
			printf("mn.x = %d ;", mn.x);
			printf("mn.y = %d ;\n", mn.y);
			masB[mn.x][mn.y] = mn.memory;
		}
	}
}

void sig_alarm(int signum){
	signal(SIGALRM,last_out);
	printf("\nALARM\nALARM\nALARM\n");
	msgctl(msgid, IPC_RMID, NULL);
	exit(EXIT_FAILURE);
}

void ortd(int i){
	int buff = 0, fl;
	for(int j = 0; j < n; j++){
		fl = 1; 
		for(int h = n-1; h > j; h--){
			if(masA[h][i] > masA[h-1][i]){
				buff = masA[h][i];
				masA[h][i] = masA[h-1][i];
				masA[h-1][i] = buff;
				fl = 0;
			}
		}
		if(fl == 1){
				break;
		}
	}
	for(int g = 0; g < n; g++){
		printf("number = %d; ",masA[g][i]);
		mn.x = g; // номер строки
		printf("fun-mn.x = %d ;", mn.x);
		mn.y = i; // номер столбца
		printf("fun-mn.y = %d ;\n", mn.y);
		mn.memory = masA[g][i];
		msgsnd(msgid,&mn,sizeof(struct mem_mas),0);
	}
}

int main(){
signal(SIGALRM,sig_alarm);
int buf,alarm_nmbr;
printf("Press alarm number: ");
scanf("%d",&alarm_nmbr);
alarm(alarm_nmbr);
msgid = msgget(IPC_PRIVATE, IPC_CREAT|0666);
if (msgid<0) { fprintf(stdout,"\nОшибка"); return 0; }
printf("Press n: ");
scanf("%d",&n);
while(n < 0){
	printf("\nPress n again: ");
	scanf("%d",&n);
}
printf("\nPress k: ");
scanf("%d",&k);
while(k < 0){
	printf("\nPress k again: ");
	scanf("%d",&k);
}
printf("\nPress elements masA:\n");
for(int i = 0; i < n; i++){
	for(int j = 0; j < k; j++){
		scanf("%d",&buf);
		masA[i][j] = buf;
	}
}
printf("\nmasA:\n");
for(int i = 0; i < n; i++){
	for(int j = 0; j < k; j++){
	buf = masA[i][j];
		printf("%d ", buf);
	}
	printf("\n");
}

for (int i=0; i < k; i++)
{ 
	if (fork() == 0){
		ortd(i);
		sleep(3);
		kill(getppid(),SIGUSR1);
		return 1;
	}
}
signal(SIGUSR1,last_out);
pause();

printf("\nresult masA:\n");
for(int i = 0; i < n; i++){
	for(int j = 0; j < k; j++){
	buf = masB[i][j];
		printf("%d ", buf);
	}
	printf("\n");
}
msgctl(msgid, IPC_RMID, NULL);
return 0;
}
