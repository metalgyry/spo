// gcc -o /home/egor/Desktop/8lab  '/home/egor/Desktop/8lab.c' -lm
// /home/egor/Desktop/8lab

#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <sched.h>
#include <linux/sched.h>
#include <unistd.h>
#define NUMSTACK 5000
char stack[100][NUMSTACK];
int param[100];
int results=0;
char str[255];
int r;

int func(void *param){
	int par = *((int*)param);
	if(str[r-par] == str[r+par]){
		results+=1;
	}
}

int main(){
int pid,dlstr,res=0, itera;
printf("Press string: ");
fgets(str, 255, stdin);
printf("\n%s", str);
dlstr = strlen(str)-1;
printf("\nPress r: ");
scanf("%d",&r);
if(r < 0 || r > dlstr){
	scanf("%d",&r);
}
if((r-1)>(dlstr-r)){
	itera = (dlstr-r);
}
if((r-1)<(dlstr-r)){
	itera = (r-1);
}
if((r-1)==(dlstr-r)){
	itera = (r-1);
}
r-=1;
printf("r = %c", str[r]);
for (int i=1 ; i < itera+1 ; i++){
	param[i-1]=i;
	char *tostack=stack[i-1];
	clone(func,(void*)(tostack+NUMSTACK-1),CLONE_VM,(void*)(param+(i-1)));
}
param[itera]=itera;
char *tostack=stack[itera];
clone(func,(void*)(tostack+NUMSTACK-1),CLONE_VM|CLONE_VFORK,(void*)(param+itera));
printf("\nres: %d\n", results);
if(results == itera){
	printf("\nRight\n");
	return 0;
}
printf("\nWrong\n");
return 0;
}
